using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    [Header("Mouse Controller")]
    [SerializeField] RectTransform ctrlHandlerRect;
    [SerializeField] Button jumpBtn;
    Vector2 ctrlCenterPos;
    float ctrlRadius;

    [Header("Player")]
    [SerializeField] GameObject player;
    [SerializeField] float speed;
    [SerializeField] float jumpUpForce;
    Rigidbody playerRb;
    const float groundDistance = 0.6f;
    float moveForce;

    bool isPointerDown;

    void Awake()
    {
        if (Application.isMobilePlatform)
            Destroy(this);
    }

    void Start()
    {
        RectTransform ctrlRect = GetComponent<RectTransform>();
        playerRb = player.GetComponent<Rigidbody>();

        // Calculat controller radius and center position
        ctrlRadius = ctrlRect.rect.width / 2;
        ctrlCenterPos = ctrlRect.position + (Vector3.right * ctrlRadius);

        // Jump button event
        jumpBtn.onClick.AddListener(() =>
        {
            RaycastHit hit;
            Ray downRay = new Ray(player.transform.position, -Vector3.up);
            Physics.Raycast(downRay, out hit);

            if (hit.distance < groundDistance)
            {
                playerRb.AddForce(Vector3.up * jumpUpForce, ForceMode.Impulse);
                playerRb.AddRelativeForce(Vector3.forward * moveForce, ForceMode.Impulse);
            }
        });
    }

    void FixedUpdate()
    {
        if (isPointerDown)
        {
            float distance = Vector2.Distance(Input.mousePosition, ctrlCenterPos);

            if (distance < ctrlRadius)
            {
                Move(Input.mousePosition);
                MoveHandler(Input.mousePosition);
            }
            else
            {
                Vector2 edgePos = Vector2.Lerp(ctrlCenterPos, Input.mousePosition, ctrlRadius / distance);

                Move(edgePos);
                MoveHandler(edgePos);
            }
        }
    }

    void Move(Vector2 mousePos)
    {
        // Calculate direction & force
        Vector2 distance = mousePos - ctrlCenterPos;
        float inputX = distance.x / ctrlRadius;
        float inputY = distance.y / ctrlRadius;

        Vector3 dir = new Vector3(inputX, 0, inputY).normalized;
        if (dir != Vector3.zero)
            player.transform.forward = dir;

        moveForce = (Mathf.Abs(inputX) + Mathf.Abs(inputY)) / 2;

        // Move
        player.transform.Translate(Vector3.forward * moveForce * speed * Time.deltaTime);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isPointerDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isPointerDown = false;
        MoveHandler(ctrlCenterPos);
    }

    void MoveHandler(Vector2 pos)
    {
        ctrlHandlerRect.position = pos;
    }
}
