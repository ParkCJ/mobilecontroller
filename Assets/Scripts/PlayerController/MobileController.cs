using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MobileController : MonoBehaviour
{
    [Header("Mobile Controller")]
    [SerializeField] GameObject ctrl;
    [SerializeField] RectTransform ctrlHandlerRect;
    [SerializeField] Button jumpBtn;
    Vector2 ctrlCenterPos;
    float ctrlRadius;

    [Header("Player")]
    [SerializeField] GameObject player;
    [SerializeField] float speed;
    [SerializeField] float jumpUpForce;
    Rigidbody playerRb;
    const float groundDistance = 0.6f;
    float moveForce;

    void Awake()
    {
        if (!Application.isMobilePlatform)
            Destroy(this);
    }

    void Start()
    {
        RectTransform ctrlRect = ctrl.GetComponent<RectTransform>();
        playerRb = player.GetComponent<Rigidbody>();

        // Calculat controller radius and center position
        ctrlRadius = ctrlRect.rect.width / 2;
        ctrlCenterPos = ctrlRect.position + (Vector3.right * ctrlRadius);

        // Jump button event
        jumpBtn.onClick.AddListener(() =>
        {
            RaycastHit hit;
            Ray downRay = new Ray(player.transform.position, -Vector3.up);
            Physics.Raycast(downRay, out hit);

            if (hit.distance < groundDistance)
            {
                playerRb.AddForce(Vector3.up * jumpUpForce, ForceMode.Impulse);
                playerRb.AddRelativeForce(Vector3.forward * moveForce, ForceMode.Impulse);
            }
        });
    }

    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            float distance = Vector2.Distance(touch.position, ctrlCenterPos);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (distance < ctrlRadius)
                    {
                        Move(touch.position);
                        MoveHandler(touch.position);
                    }
                    break;
                case TouchPhase.Stationary:
                case TouchPhase.Moved:
                    if (distance < ctrlRadius)
                    {
                        Move(touch.position);
                        MoveHandler(touch.position);
                    }
                    else
                    {
                        Vector2 edgePos = Vector2.Lerp(ctrlCenterPos, touch.position, ctrlRadius / distance);

                        Move(edgePos);
                        MoveHandler(edgePos);
                    }
                    break;
                case TouchPhase.Ended:
                    MoveHandler(ctrlCenterPos);
                    break;
            }
        }
    }

    void Move(Vector2 touchPos)
    {
        // Calculate direction & force
        Vector2 distance = touchPos - ctrlCenterPos;
        float inputX = distance.x / ctrlRadius;
        float inputY = distance.y / ctrlRadius;

        Vector3 dir = new Vector3(inputX, 0, inputY).normalized;
        if (dir != Vector3.zero)
            player.transform.forward = dir;

        moveForce = (Mathf.Abs(inputX) + Mathf.Abs(inputY)) / 2;

        // Move
        player.transform.Translate(Vector3.forward * moveForce * speed * Time.deltaTime);
    }

    void MoveHandler(Vector2 pos)
    {
        ctrlHandlerRect.position = pos;
    }
}